package minesweeper;

public class Coord {
	private int row;
	private int col;
	private int nearbyMines = 0;
	private boolean mine = false;
	private boolean flagged = false;
	private boolean cleared = false;

	public Coord(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	public boolean isCleared() {
		return cleared;
	}

	public void setCleared(boolean cleared) {
		this.cleared = cleared;
	}

	public boolean isFlagged() {
		return flagged;
	}

	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public int getNearbyMines() {
		return nearbyMines;
	}

	public void setNearbyMines(int nearbyMines) {
		this.nearbyMines = nearbyMines;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	@Override
	public String toString() {
		return "row = " + row + " col = " + col + " nearby mines: " + nearbyMines + " flagged = " + flagged;
	}

	public boolean equals(Coord c) {
		if (row == c.getRow() && col == c.getCol()) {
			return true;
		}
		return false;
	}

	public void incNearbyMines() {
		++nearbyMines;
	}

}
