package minesweeper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class MineSweeper extends Application {
	private Stage primaryStage;
	int rows = 10;
	int cols = 10;
	private int buttonHeight = 70;
	private GridPane gridPane = new GridPane();

	ArrayList<ArrayList<Button>> buttonList = new ArrayList<ArrayList<Button>>();
	private List<Coord> mineList = new ArrayList<Coord>();

	private int numberOfMines = 20;

	public MineSweeper() {

	}

	@Override
	public void start(Stage stage) {
		this.primaryStage = stage;
		initialize();

		StackPane root = new StackPane();
		root.getChildren().add(gridPane);

		Scene scene = new Scene(root, 800, 800);

		primaryStage.setTitle("Mine sweeper");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	private void initialize() {

		for (int i = 0; i < rows; ++i) {
			ArrayList<Button> butList = new ArrayList<Button>();
			buttonList.add(butList);

			for (int j = 0; j < cols; ++j) {
				Button but = new Button();
				but.setStyle("-fx-background-color: #999999");
				butList.add(but);
				but.setMinHeight(buttonHeight);
				but.setMinWidth(buttonHeight);
				but.setMaxHeight(buttonHeight);
				but.setMaxWidth(buttonHeight);

				boolean flag;
				if (i % 2 == 0) {
					if (j % 2 == 0) {
						flag = true;
					} else {
						flag = false;
					}
				} else {
					if (j % 2 == 0) {
						flag = false;
					} else {
						flag = true;
					}
				}

				if (flag) {
					but.setStyle("-fx-background-color: #999999");
				} else {
					but.setStyle("-fx-background-color: #AAAAFF");
				}

				Coord coord = new Coord(i, j);
				but.setUserData(coord);
				gridPane.add(but, j, i);
				but.setOnMouseClicked(event -> clicked(event));
			}
			System.out.println();
		}

		placeMines();
		// setButtonSize();
		computeNearbyMines();

	}

	private void computeNearbyMines() {
		for (int row = 0; row < rows; ++row) {
			for (int col = 0; col < cols; ++col) {
				Coord coord = (Coord) (getButton(row, col).getUserData());
				if (coord.isMine() == false) {
					List<Coord> neighbours = findNeighbours(coord);
					for (Coord neigh : neighbours) {
						if (neigh.isMine()) {
							coord.incNearbyMines();
						}
					}
				}
			}
		}
	}

	private List<Coord> findNeighbours(Coord coord) {
		List<Coord> neighbours = new ArrayList<Coord>();

		int minRow = Math.max(coord.getRow() - 1, 0);
		int maxRow = Math.min(coord.getRow() + 1, rows - 1);
		int minCol = Math.max(coord.getCol() - 1, 0);
		int maxCol = Math.min(coord.getCol() + 1, cols - 1);

		for (int row = minRow; row <= maxRow; ++row) {
			for (int col = minCol; col <= maxCol; ++col) {
				if (!(row == coord.getRow() && col == coord.getCol())) {
					Coord neigh = (Coord) (getButton(row, col).getUserData());
					neighbours.add(neigh);
				}
			}
		}
		return neighbours;
	}

	private void setButtonSize() {
		Coord mine1 = mineList.get(0);

		Button but = getButton(mine1.getRow(), mine1.getCol());
		double height = but.getHeight();
		double width = but.getWidth();

		System.out.println("min height is " + height + " min width is " + width);
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				getButton(i, j).setMinHeight(height);
				getButton(i, j).setMinWidth(width);
				getButton(i, j).setMaxHeight(height);
				getButton(i, j).setMaxWidth(width);
			}
		}
	}

	private void placeMines() {
		Random rand = new Random(123);
		int placedMines = 0;
		placeMine: do {
			int row = rand.nextInt(rows);
			int col = rand.nextInt(cols);
			Coord coord = (Coord) (getButton(row, col).getUserData());
			for (Coord mineCoord : mineList) {
				if (mineCoord.equals(coord)) {
					continue placeMine;
				}
			}
			mineList.add(coord);
			coord.setMine(true);
			++placedMines;

			Image img = new Image("bomb.png");
			ImageView view = new ImageView(img);
			// view.setFitHeight(buttonHeight);
			// view.setPreserveRatio(true);

			getButton(row, col).setGraphic(view);

		} while (placedMines < numberOfMines);
	}

	private Button getButton(int row, int col) {
		return buttonList.get(row).get(col);
	}

	private void clicked(MouseEvent event) {
		Coord coord = (Coord) ((Button) event.getSource()).getUserData();
		System.out.println("coord is " + coord.toString());
		Button but = getButton(coord.getRow(), coord.getCol());
		if (event.getButton() == MouseButton.PRIMARY) {
			System.out.println("left button clicked");

			System.out.println(coord);
			System.out.println("========================= neighbours ===========================");
			List<Coord> neighbours = findNeighbours(coord);
			for (Coord c : neighbours) {
				System.out.println(c);
			}

			if (coord.isMine()) {
				System.out.println("you stepped on a mine");
				return;
			}

			if (!coord.isCleared()) {
				checkClick(coord);
			}

		} else if (event.getButton() == MouseButton.SECONDARY) {
			System.out.println("right button clicked");
			if (coord.isFlagged()) {
				// clear the flag
				but.setGraphic(null);
				coord.setFlagged(false);
			} else {
				// set the flag
				Image img = new Image("flag.png");
				ImageView view = new ImageView(img);
				view.setFitHeight(buttonHeight);
				view.setPreserveRatio(true);

				but.setGraphic(view);
				coord.setFlagged(true);
			}
		}
	}

	private void checkClick(Coord coord) {

		List<Coord> sideNeighbours = findNeighbours(coord);
		Button but = getButton(coord.getRow(), coord.getCol());
		but.setStyle("-fx-background-color: #CCCCCC");
		but.setGraphic(null);

		coord.setCleared(true);
		System.out.println("clearing " + coord);

		if (coord.getNearbyMines() > 0) {
			but = getButton(coord.getRow(), coord.getCol());
			but.setGraphic(null);
			but.setText(Integer.toString(coord.getNearbyMines()));
			but.setStyle("-fx-background-color: #CCCCCC");
		} else {
			for (Coord c : sideNeighbours) {
				if (!c.isMine() && !c.isCleared()) {
					System.out.println("recursing");
					checkClick(c);
				}
			}
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
